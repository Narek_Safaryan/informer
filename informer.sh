#! /bin/bash
function PROCESSOR {
echo -e "\033[1;31;4m--PROCESSOR--\033[m"
PROCNUMBER=`cat /proc/cpuinfo | grep -c "processor"`
echo "PROCESSOR_NUMBER : $PROCNUMBER"
VENDOR_ID=`cat /proc/cpuinfo | grep  "vendor_id"  | head -1`
echo "$VENDOR_ID"
MODEL_NAME=`cat /proc/cpuinfo | grep  "model name" | head -1`
echo "$MODEL_NAME"
CPU_FAMILY=`cat /proc/cpuinfo | grep  "cpu family" | head -1`
echo "$CPU_FAMILY"
FPU=`cat /proc/cpuinfo | grep  "fpu" | head -1`
echo "$FPU" 
}
function RAM {
echo -e "\033[1;31;4m--RAM--\033[m"
MEM=`free -h | grep  "Mem"`
echo  "Total=`echo $MEM | cut -d " " -f2`"
echo "Used=`echo $MEM | cut -d " " -f3`" 
echo "Free=`echo $MEM | cut -d " " -f4`"
}
function DISK {
echo -e "\033[1;31;4m--DISK--\033[m"
DISK=`lsblk -r  | grep "sd" | cut -d " " -f1,4`
echo "$DISK"
}
function NETWORK {
echo -e "\033[1;31;4m--NETWORK--\033[m"
ETH=` ifconfig | grep "eth"`
if [ -n "$ETH" ]
then
 echo "eth     yes"
else
 echo "eth     no"
fi
WLAN=` ifconfig | grep "wlan"`
if [ -n "$WLAN" ]
 then
echo "wlan    yes"
 else
echo "wlan    no"
fi
}
function SYSTEM {
echo -e "\033[1;31;4m--SYSTEM--\033[m"
UNAME=`uname --all | cut -d " " -f1,3,4,14,15 `
echo  "$UNAME"
}
function DISPLAY {
echo -e "\033[1;31;4m--DISPLAY--\033[m"
SCREEN=`xrandr | grep  "Screen"`
echo "$SCREEN"
DVI=`xrandr | grep  "DVI" | cut -d " " -f1,2`
echo "$DVI"
VGA=`xrandr | grep  "VGA" | cut -d " " -f1,2`
echo "$VGA"
}
for i in $*
do 
if [ $i == "PROCESSOR" -o $i == "RAM" -o $i == "DISK" -o $i == "NETWORK" -o $i == "SYSTEM" -o $i == "DISPLAY" ]
then 
$i
else 
if [ $* == "--all" ]
then
PROCESSOR
RAM
DISK
NETWORK
SYSTEM
DISPLAY
else
echo -e "\033[1;31;4mTry using hildren commands-PROCESSOR_RAM_DISK_NETWORK_SYSTEM_DISPLAY_--all\033[m"
fi
fi
done




