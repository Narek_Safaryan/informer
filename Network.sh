#! /bin/bash
t=1
p=0
while [ $t -le 30 ]
do
 p="192.168.1.$t"
 t=`expr $t + 1`
 a=`ping -c 1 $p | grep  "1 received,"`
 if [ "$a" ]
  then
   echo -e "\033[34;41;4m $p \033[0m"
 fi
done
